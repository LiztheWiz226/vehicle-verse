from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .models import Sale, Salesperson, Customer, AutomobileVO
from .encoders import SalespersonListEncoder, SalespersonDetailEncoder, CustomerListEncoder, CustomerDetailEncoder, Sales_List_Encoder

# Create your views here.














@require_http_methods(["GET", "POST"])
def Salesperson_list(request):

    if request.method == "GET":
                salesperson = Salesperson.objects.all()
                return JsonResponse(
                {"salesperson": salesperson},
                encoder=SalespersonListEncoder,
            )

    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the salesperson"}
            )
            response.status_code = 404
            return response



@require_http_methods(["GET", "DELETE", "PUT"])
def Show_salesperson(request, id):

    if request.method == "GET":
        customer = Salesperson.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )


    elif request.method == "DELETE":

        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})





@require_http_methods(["GET", "POST"])
def Customer_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the customer"}
            )
            response.status_code = 404
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def Show_customer(request, id):

    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


    elif request.method == "DELETE":

        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})




@require_http_methods(["GET", "POST"])
def Sales_list(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=Sales_List_Encoder,



        )
    else:
        content = json.loads(request.body)
        try:

            automobile = AutomobileVO.objects.get(id=content["automobile"])
            content["automobile"] = automobile

            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer

            salesperson = Salesperson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson



        except Salesperson.DoesNotExist or Customer.DoesNotExist or AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id or customer id"},
                status=400,


            )

        sale = Sale.objects.create(**content)


        return JsonResponse(
                sale,
                encoder=Sales_List_Encoder,
                safe=False,
            )
