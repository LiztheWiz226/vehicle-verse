from django.urls import path

from .views import Salesperson_list, Show_salesperson, Customer_list, Show_customer, Sales_list

urlpatterns = [
    path("salespeople/", Salesperson_list, name="Salesperson_list"),
    path("salespeople/<int:id>/", Show_salesperson, name="Delete_salesperson"),
    path("customers/", Customer_list, name="Customer_list"),
    path("customers/<int:id>/", Show_customer, name="Delete_customer"),
    path("sales/", Sales_list, name="Sales_list"),

]
