from django.contrib import admin
from .models import Customer, Salesperson, Sale, AutomobileVO

# Register your models here.
@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = (
    "first_name",
    "last_name",
    "id",
    )

@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    list_display = (
    "first_name",
    "last_name",
    "id",
    )


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = (
        "vin",
        "id",
    )


@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = (
        "automobile",
        "salesperson",
        "customer",
        "price",
        "id",
    )
