# CarCar

Team:

* Timothee Mbutcho - Sales
* Liz Ward - Service

## Design

## Service microservice

The service microservice incldues models for Technician, Service Appointments, Status, and AutomobileVO (value object).

### Service models
* Technician model incldues:
    - first_name
    - last_name
    - employee_id
        - must be unique

* Appointment model incldues:
    - date_time
    - reason
    - vin
        - enter the VIN for the automobile to be serviced; does not need to be unique since any automobile can be serviced multiple times
        - note: if the automobile VIN matches an auto that was once in inventory (i.e. sold from this dealership), the Service Appointment will be flagged as "VIP" in the Service Appointments lists
    - customer
        - enter the customer name for the service appointment; does not need to match customers from Sales
    - technician (uses foreign key)
    - status (uses foreign key)

* Status is used to capture the status of Service Appointments. Current statuses are: Created (defualt), Finished, and Canceled
    - name

* AutomobileVO polls the Automobile object in CarCar Inventory every 60 seconds, to get the automobile VIN for all autombobiles:
    - vin


### Service web pages

* Separate web pages are available for users to:
    * Technicians
        * View all technicians
        * Create a new technician
    * Service Appointments
        * View all current "open" appointments (i.e. "created" appointments; not canceled or finished)
            * Users can modify the status of open appointments from this page mark appointments as Canceled or Finished from this page
            * Appointments associated with cars that were once in invenotory (per their VIN) are flagged as "VIP"
        * View history of all service appointments, current and previous
            * Users can serach by VIN in the search box to view only appointments related to a particular VIN
            * Appointments associated with cars that were once in invenotory (per their VIN) are flagged as "VIP"
        * Create new Service Appointments



## Sales microservice

Explain your models and integration with the inventory
microservice, here.
