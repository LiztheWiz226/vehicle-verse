from django.urls import path

from .api_views import (
    api_technicians,
    api_technician,
    api_appointments,
    api_appointment,
    api_finish_appointment,
    api_cancel_appointment,
    api_automobileVOs,
)

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("technicians/<int:id>/", api_technician, name="api_technician"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:id>/", api_appointment, name="api_appointment"),
    path("appointments/<int:id>/finish", api_finish_appointment, name="api_finish_appointment"),
    path("appointments/<int:id>/cancel", api_cancel_appointment, name="api_cancel_appointment"),
    path("automobileVOs/", api_automobileVOs, name="api_automobileVOs"),
]
