from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(
        max_length=100, unique=True
    )

    def __str__(self):
        return self.first_name + " " + self.last_name


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class Status(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)  # Default ordering for Status
        verbose_name_plural = "statuses"  # Set the pluralization


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.ForeignKey(
        Status,
        related_name="appointments",
        on_delete=models.PROTECT,
        default=1,
    )

    # VIN here does not need to be unique; can make multiple appointments per VIN
    vin = models.CharField(max_length=17)

    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.vin

    def finish(self):
        status = Status.objects.get(name="Finished")
        self.status = status
        self.save()

    def cancel(self):
        status = Status.objects.get(name="Canceled")
        self.status = status
        self.save()

    class Meta:
        ordering = ("id",)  # Default ordering for appointments
