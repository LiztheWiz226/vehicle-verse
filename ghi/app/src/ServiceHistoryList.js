import React, { useState, useEffect } from 'react';

function ServiceHistoryList() {
    const [appointments, setAppointments] = useState([])
    const [automobileVOs, setAutomobileVOs] = useState([])
    const [filterValue, setFilterValue] = useState("");

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }

    const getAutoVOData = async () => {
        const response = await fetch('http://localhost:8080/api/automobileVOs/');

        if (response.ok) {
            const data = await response.json();
            setAutomobileVOs(data.automobileVOs)
        }
    }

    const handleFilterValueChange = (e) => {
        const value = e.target.value;
        console.log("value:", value);
        setFilterValue(value);
    };

    useEffect(() => {
        getData()
        getAutoVOData()
    }, [])

    function checkIfVIP(vin) {
        for (const autoVO of automobileVOs) {
            if (vin === autoVO.vin) {
                return "Yes";
            }
        }
        return "No";
    }


    return (
        <div>
            <h1>Service History</h1>
            <div>
                <input
                    onChange={handleFilterValueChange}
                    value={filterValue}
                    placeholder="Search by VIN..."
                />
                <button type="button">Search</button>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time (24:00)</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments
                        .filter((appt) => appt.vin.includes(filterValue))
                        .map(appointment => {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.id} </td>
                                    <td>{appointment.vin}</td>
                                    <td>{checkIfVIP(appointment.vin)}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{String(appointment.date_time.split("T")[0])}</td>
                                    <td>{String(appointment.date_time.split("T")[1]).split("+")[0]}</td>
                                    <td>{appointment.technician.first_name} {appointment.technician.last_name} ({appointment.technician.employee_id})</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.status}</td>
                                </tr>
                            );
                        }
                        )}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceHistoryList
