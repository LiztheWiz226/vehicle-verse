import React, { useState, useEffect } from 'react';

function AppointmentsList() {
    const [appointments, setAppointments] = useState([])
    const [automobileVOs, setAutomobileVOs] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }

    const getAutoVOData = async () => {
        const response = await fetch('http://localhost:8080/api/automobileVOs/');

        if (response.ok) {
            const data = await response.json();
            setAutomobileVOs(data.automobileVOs)
        }
    }

    useEffect(() => {
        getData()
        getAutoVOData()
    }, [])

    const handleStatusMod = async (event, id, statusMod) => {
        event.preventDefault();
        const appointmentUrl = `http://localhost:8080/api/appointments/${id}/${statusMod}`;

        const fetchConfig = {
            method: "put",
        }

        const response = await fetch(appointmentUrl, fetchConfig);

        if (response.ok) {
            console.log(`${statusMod}ed appointment id # ${id}`)
            console.log(response)

            // Filter appointment list to remove the finished/canceled item upon click (otherwise requires refresh)
            setAppointments(appointments.filter((appt) => appt.id !== id))
        }
    };

    function checkIfVIP(vin) {
        for (const autoVO of automobileVOs) {
            if (vin === autoVO.vin) {
                return "Yes";
            }
        }
        return "No";
    }

    return (
        <div>
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time (24:00)</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Modify Appointment</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments
                        .filter((appt) => appt.status === "Created") // Only show open appointments (status = 'Created' only; not Finished or Canceled)
                        .map(appointment => {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.id} </td>
                                    <td>{appointment.vin}</td>
                                    <td>{checkIfVIP(appointment.vin)}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{String(appointment.date_time.split("T")[0])}</td>
                                    <td>{String(appointment.date_time.split("T")[1]).split("+")[0]}</td>
                                    <td>{appointment.technician.first_name} {appointment.technician.last_name} ({appointment.technician.employee_id})</td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                        <button onClick={event => handleStatusMod(event, appointment.id, "cancel")}
                                            type="button" className="btn btn-danger">Cancel</button>
                                        <button onClick={event => handleStatusMod(event, appointment.id, "finish")}
                                            type="button" className="btn btn-success">Finish</button>
                                    </td>
                                </tr>
                            );
                        }
                        )}
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentsList
