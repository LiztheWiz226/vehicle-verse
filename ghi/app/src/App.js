import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import TechniciansList from './TechniciansList';
import AppointmentForm from './AppointmentForm';
import AppointmentsList from './AppointmentsList';
import ServiceHistoryList from './ServiceHistoryList';
import VehicleModelForm from './VehicleModelsForm';
import VehicleModelsList from './VehicleModelsList';

import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import AutomobileForm from './AutomobileForm';
import AutomobilesList from './AutomobilesList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers/">
            <Route index element={<ManufacturersList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="automobiles/">
            <Route index element={<AutomobilesList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="technicians/">
            <Route index element={<TechniciansList />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>

          <Route path="models/">
            <Route index element={<VehicleModelsList />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>

          <Route path="appointments/">
            <Route index element={<AppointmentsList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<ServiceHistoryList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
