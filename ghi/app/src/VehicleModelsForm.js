import React, {useState, useEffect} from 'react';

function VehicleModelForm () {
  const [models, setModels] = useState([])
  const [manufacturers, setManufacturers] = useState([])
  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
    manufacturer: '',
  })

  const getData = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }

  const getManufacturerData = async () => {
    const response = await fetch('http://localhost:8100/api/manufacturers/');

    if (response.ok) {
        const data = await response.json();
        console.log(data)
        setManufacturers(data.automobileVOs)
    }
}






  useEffect(()=> {
    getData()
    getManufacturerData()
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault();

    const modelUrl = 'http://localhost:8100/api/models/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(modelUrl, fetchConfig);

    if (response.ok) {
      setFormData({
      name: '',
      picture_url: '',
      manufacturer: '',
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({

      ...formData,


      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new vehicle model</h1>
          <form onSubmit={handleSubmit} id="create-model-form">
            <div className="form-floating mb-3">
              <input value={formData.name} onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formData.picture_url} onChange={handleFormChange} placeholder="picture_url" required type="url" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture_url</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formData.manufacturer} onChange={handleFormChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>



            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default VehicleModelForm;
