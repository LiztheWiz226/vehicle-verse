import React, { useState, useEffect } from 'react';

function TechniciansList() {
    const [technicians, setTechnicians] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

    useEffect(() => { getData() }, [])

    const handleDelete = async (event, id) => {
        event.preventDefault();

        const technicianUrl = `http://localhost:8080/api/technicians/${id}`;
        const fetchConfig = { method: "delete", }
        const response = await fetch(technicianUrl, fetchConfig);

        if (response.ok) {
            console.log(`Deleted technician id # ${id}`)

            // Filter technicians list to remove the deleted tech upon click (otherwise requires refresh)
            setTechnicians(technicians.filter((tech) => tech.id !== id))
        }
    };


    return (
        <div>
            <h1>Technicians</h1>
            <a className="btn btn-primary" href='http://localhost:3000/technicians/new' role="button">Add a new technician</a>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                        <th>Delete technician</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians
                        .map(technician => {
                            return (
                                <tr key={technician.id}>
                                    <td>{technician.id}</td>
                                    <td>{technician.first_name}</td>
                                    <td>{technician.last_name}</td>
                                    <td>{technician.employee_id}</td>
                                    <td>
                                        <button
                                            onClick={event => handleDelete(event, technician.id)}
                                            type="button" className="btn btn-danger">Delete
                                        </button>
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

export default TechniciansList
